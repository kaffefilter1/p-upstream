
-- Commit everything in one go
BEGIN;

CREATE TABLE public.area (
	id SERIAL PRIMARY KEY,
	client INT,
	description TEXT NOT NULL DEFAULT '',
	name TEXT NOT NULL DEFAULT '',

	-- Will be set to false instead of delete
	active BOOLEAN NOT NULL DEFAULT TRUE,

	address TEXT NOT NULL DEFAULT ''
);

CREATE TABLE public.spot (
	id SERIAL PRIMARY KEY,
	area INT NOT NULL REFERENCES public.area(id) ON DELETE CASCADE,

	-- Location ting
	location POINT NOT NULL DEFAULT '(0,0)',
	floor INT NOT NULL DEFAULT 0,

	-- Will be referenced in the parking.conditions
	flags text[] NOT NULL DEFAULT '{}'
);

-- This will help keep statictics
CREATE TABLE public.update (
	spot INT NOT NULL REFERENCES public.spot(id) ON DELETE CASCADE,
	measured TIMESTAMP NOT NULL,
	available BOOLEAN,

	PRIMARY KEY(spot, measured)
);

COMMIT;
