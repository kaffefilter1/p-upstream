package main

/*
Dependencies:
github.com/gorilla/mux
github.com/lib/pq

Install by using "go get -u github.com/lib/pq" & "go get -u github.com/gorilla/mux" & "go get -u github.com/jmoiron/sqlx"
*/

import (
	"errors"
	"fmt"
	"log"
	"time"

	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

type PAC struct {
	Id uint
}

type Config struct {
	DBstr    string `json:"dbstr"`
	HttpAddr string `json:"httpaddr"`
}

type App struct {
	Router *mux.Router
	DB     *sqlx.DB

	conf *Config
	log  *log.Logger
}

// Custom time type using a different time format with json
type Time struct {
	time.Time
}

const timeformat = "2006-01-02T15:04:05"

func (t *Time) UnmarshalJSON(b []byte) error {
	// The wierd indexing is to remove the "" around the time.
	pt, err := time.Parse(timeformat, string(b[1:len(b)-1]))
	if err != nil {
		return err
	}

	t.Time = pt
	return nil
}

func (t Time) MarshalJSON() ([]byte, error) {
	str := fmt.Sprintf("\"%s\"", t.Format(timeformat))

	return []byte(str), nil
}

func (t Time) String() string {
	return t.Format(timeformat)
}

func (a *App) Initialize(conf *Config, logger *log.Logger) error {

	a.conf = conf
	a.log = logger

	var err error
	a.DB, err = sqlx.Connect("postgres", conf.DBstr)
	if err != nil {
		return fmt.Errorf("init database: %w", err)
	}

	// Ping
	if err := a.DB.Ping(); err != nil {
		return fmt.Errorf("ping database: %w", err)
	}

	a.Router = mux.NewRouter()

	a.initializeRoutes()
	return nil
}

func (a *App) Run() {
	// Print to the user where we are listening
	a.log.Printf("Listening on %s", a.conf.HttpAddr)

	// Listen on http
	err := http.ListenAndServe(a.conf.HttpAddr, a.Router)
	if err != nil {
		a.log.Fatal(err)
	}
}

func (a *App) respondWithError(w http.ResponseWriter, code int, message string) {
	a.log.Printf("HTTP error %d: %s", code, message)
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func (a *App) areaReport(w http.ResponseWriter, r *http.Request) {
	// API request structure
	var input struct {
		Measured   Time `json:"ms"`
		SpotStates []struct {
			Spotid    uint `json:"id"`
			Available bool `json:"av"`
		} `json:"ss"`
	}

	// Load request data into input variable.
	// Setup decoder to disallow unknownfields
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	if err := dec.Decode(&input); err != nil {
		a.respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	a.log.Printf("Report with data %+v", input)

	// Do all sql things in a single transaction
	txn, err := a.DB.BeginTx(r.Context(), nil)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer txn.Rollback()

	// Save all the data as a bulk update
	// Prepare the sql statement
	stmt, err := txn.Prepare("INSERT INTO update(spot, measured, available) VALUES ( $1, $2, $3 )")
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer stmt.Close()

	stmtNotify, err := txn.Prepare("SELECT pg_notify('postgraphile:spotUpdated', json_build_object('__node__', json_build_array('Spot', $1::INT))::text)")
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer stmtNotify.Close()

	// Apply spots using the prepared statement
	for _, spot := range input.SpotStates {
		_, err := stmt.Exec(spot.Spotid, input.Measured.Time, spot.Available)
		if err != nil {
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		_, err = stmtNotify.Exec(spot.Spotid)
		if err != nil {
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
	}

	// Commit the changes
	if err := txn.Commit(); err != nil {
		a.respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
}

func (a *App) register(w http.ResponseWriter, r *http.Request) {
	var input []struct {
		Location []float32      `json:"location"`
		Floor    int            `json:"floor"`
		Flags    pq.StringArray `json:"flags"`
	}

	areaidstr, ok := mux.Vars(r)["id"]
	if !ok {
		a.respondWithError(w, http.StatusBadRequest, "Please specify id")
		return
	}

	areaid, err := strconv.ParseUint(areaidstr, 10, 64)
	if err != nil {
		a.respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	// Parse post input
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	if err := dec.Decode(&input); err != nil {
		a.respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	// Validate the location array
	for i, spot := range input {
		if len(spot.Location) != 2 {
			// If not a valid position default to NULL
			input[i].Location = []float32{0, 0}
		}
	}

	// Insert spots into database in a single transaction
	txn, err := a.DB.BeginTx(r.Context(), nil)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer txn.Rollback()

	stmt, err := txn.Prepare(`INSERT INTO spot(area, location, floor, flags)
		VALUES ($1, Point($2, $3), $4, $5) RETURNING id`)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer stmt.Close()

	// Insert changes while recording allocated ids
	var ids []uint
	for _, spot := range input {
		var id uint
		err := stmt.QueryRow(areaid, spot.Location[0], spot.Location[1], spot.Floor, spot.Flags).Scan(&id)
		if err != nil {
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		ids = append(ids, id)
	}

	// Commit the changes
	if err := txn.Commit(); err != nil {
		a.respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	// Return ids
	respondWithJSON(w, http.StatusCreated, ids)

}

func (a *App) getPACs(w http.ResponseWriter, r *http.Request) {
	count, _ := strconv.Atoi(r.FormValue("count"))
	start, _ := strconv.Atoi(r.FormValue("start"))

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	//PACs, err := getPACs(a.DB, start, count)
	err := errors.New("Not iplemented")
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	//respondWithJSON(w, http.StatusOK, PACs)
}

/*
func (a *App) getPAC(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Parking Area Client ID")
		return
	}

	p := PAC{Id: uint(id)}
	if err := p.getPAC(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Parking Area Client not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, p)
}

func (a *App) createPAC(w http.ResponseWriter, r *http.Request) {
	var p PAC
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request")
		return
	}
	defer r.Body.Close()

	if err := p.createPAC(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, p)
}

func (a *App) updatePAC(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Parking Area Client ID")
		return
	}

	var p PAC
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid resquest")
		return
	}
	defer r.Body.Close()
	p.Id = uint(id)

	if err := p.updatePAC(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, p)
}

func (a *App) deletePAC(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Parking Area Client ID")
		return
	}

	p := PAC{Id: uint(id)}
	if err := p.deletePAC(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}
*/

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/area/{id:[0-9]+}/report", a.areaReport).Methods("POST")
	a.Router.HandleFunc("/area/{id:[0-9]+}/register", a.register).Methods("POST")
	a.Router.HandleFunc("/pacs", a.getPACs).Methods("GET")
	/*
		a.Router.HandleFunc("/pac", a.createPAC).Methods("POST")
		a.Router.HandleFunc("/pac/{id:[0-9]+}", a.getPAC).Methods("GET")
		a.Router.HandleFunc("/pac/{id:[0-9]+}", a.updatePAC).Methods("PUT")
		a.Router.HandleFunc("/pac/{id:[0-9]+}", a.deletePAC).Methods("DELETE")
	*/
}
