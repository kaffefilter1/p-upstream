package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

func loadconfig(path string) (*Config, error) {
	// Load config file
	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("reading config file: %w", err)
	}
	// Remember to close the file
	defer f.Close()

	// Json decode the file
	conf := &Config{}

	jsondec := json.NewDecoder(f)
	jsondec.DisallowUnknownFields()

	err = jsondec.Decode(conf)
	if err != nil {
		return nil, fmt.Errorf("parsing config file: %w", err)
	}

	return conf, nil
}

func loadconfigenv() *Config {
	conf := &Config{
		DBstr:    os.Getenv("COLLECTOR_DBSTR"),
		HttpAddr: os.Getenv("COLLECTOR_HTTP_LISTEN"),
	}

	return conf
}

func main() {
	// Create logger
	logger := log.New(os.Stdout, "", log.Lshortfile|log.LstdFlags)

	// Parse command line arguments. Flag vil give a pointer to a string
	configFile := flag.String("c", "", "path to config file")
	flag.Parse()

	var conf *Config
	if *configFile != "" {
		var err error
		conf, err = loadconfig(*configFile)
		if err != nil {
	        logger.Fatal(err)
		}
	} else {
		conf = loadconfigenv()
	}

	a := App{}
	err := a.Initialize(conf, logger)
	if err != nil {
		logger.Fatal(err)
	}

	a.Run()
}
